const express = require("express");
const socket = require("socket.io");

const app = express();

const server = app.listen(4000, err => {
	err
		? console.log("app:server", `Failed to start server instance %O`, err)
		: console.log("app:server", `Listening on port ${4000}`);
});

// Static file
app.use(express.static("index.js"));

// Socket config
const io = socket(server);
io.on("connection", socket => {
	console.log(socket.id);

	socket.on("SEND_MESSAGE", function(data) {
		io.emit("RECEIVE_MESSAGE", data);
	});
});
